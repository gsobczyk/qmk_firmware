ifndef QUANTUM_DIR
	include ../../../../Makefile
endif
BACKLIGHT_ENABLE = yes
MOUSEKEY_ENABLE = yes
TAP_DANCE_ENABLE = yes
AUDIO_ENABLE = no
MIDI_ENABLE = no
UNICODE_ENABLE      = yes   # Unicode (can't be used with unicodemap)
UNICODEMAP_ENABLE   = no    # Enable extended unicode