/* Copyright 2015-2017 Jack Humbert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "planck.h"
#include "action_layer.h"
#include "eeconfig.h"

extern keymap_config_t keymap_config;

enum planck_layers {
  _QWERTY,
  _LOWER,
  _RAISE,
  _ADJUST,
  _MOUSE
};

enum planck_keycodes {
  QWERTY = SAFE_RANGE,
  LOWER,
  RAISE,
  BACKLIT,
  MOUSE,
  SMILE,
  SAD,
  CONFUSED,
  HMMM,
  SHRUG,
  LENNY,
  DISSAPROVAL,
  DYNAMIC_MACRO_RANGE,
};

void matrix_init_user(void) {
    set_unicode_input_mode(UC_LNX);
};

#include "dynamic_macro.h"

//Tap Dance Declarations
enum {
  TD_SFT_CAPS = 0
};

//Tap Dance Definitions
qk_tap_dance_action_t tap_dance_actions[] = {
  //Tap once for Esc, twice for Caps Lock
  [TD_SFT_CAPS]  = ACTION_TAP_DANCE_DOUBLE(KC_LSFT, KC_CAPS)
// Other declarations would go here, separated by commas, if you have them
};

// Func macro definitions.
#define SFT_ENT  FUNC(0) // Tap for Enter, hold for Shift
#define ALTG_SPC  FUNC(1) // Tap for SPace, hold for AltGr


// Function definitions
const uint16_t PROGMEM fn_actions[] = {
    [0] = ACTION_MODS_TAP_KEY(MOD_RSFT, KC_ENT),
	[1] = ACTION_MODS_TAP_KEY(MOD_RALT, KC_SPC)
};

void tap(uint16_t keycode){
    register_code(keycode);
    unregister_code(keycode);
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Qwerty
 * ,-------------------------------------------------------------------------------------.
 * | Tab  |   Q  |   W  |   E  |   R  |   T  |   Y  |   U  |   I  |   O  |   P  | Bksp   |
 * |------+------+------+------+------+-------------+------+------+------+------+--------|
 * | Esc  |   A  |   S  |   D  |   F  |   G  |   H  |   J  |   K  |   L  |   ;  |  "     |
 * |------+------+------+------+------+------|------+------+------+------+------+--------|
 * | Shift|   Z  |   X  |   C  |   V  |   B  |   N  |   M  |   ,  |   .  |  Up  |Sft/Ent |
 * |------+------+------+------+------+------+------+------+------+------+------+--------|
 * | Ctrl | Alt  | GUI  |Mouse |Lower | AltGr/Space |Raise |   /  | Left | Down | Right  |
 * `--------------------------------------------------------------------------------------'
 */
[_QWERTY] = {
  {KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,  KC_T,     KC_Y,     KC_U,    KC_I,    KC_O,    KC_P,    KC_BSPC},
  {KC_ESC,  KC_A,    KC_S,    KC_D,    KC_F,  KC_G,     KC_H,     KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT},
  {KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,  KC_N,     KC_M,     KC_COMM, KC_DOT,  KC_UP,   SFT_ENT },
  {KC_LCTL, KC_LGUI, KC_LALT, MOUSE,   LOWER, ALTG_SPC, ALTG_SPC, RAISE,   KC_SLSH, KC_LEFT, KC_DOWN, KC_RGHT}
},

/* Lower
 * ,-----------------------------------------------------------------------------------.
 * | Del  |   !  |   @  |   #  |   $  |   %  |   ^  |   &  |   *  |   (  |   )  | Bksp |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |   ~  |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |   _  |   +  |   {  |   }  |  |   |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |  F7  |  F8  |  F9  |  F10 |  F11 |  F12 |      |      | PgUp | PgUp | PgDn |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |             |      |      | Home | PgDn | End  |
 * `-----------------------------------------------------------------------------------'
 */
[_LOWER] = {
  {KC_DEL,  KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC, KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_BSPC},
  {KC_TILD, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, KC_PIPE},
  {_______, KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  _______, _______, KC_PGUP, KC_PGUP, KC_PGDN},
  {_______, _______, _______, _______, _______, _______, _______, _______, _______, KC_HOME, KC_PGDN, KC_END}
},

/* Raise
 * ,-----------------------------------------------------------------------------------.
 * | Del  |   1  |   2  |   3  |   4  |   5  |   6  |   7  |   8  |   9  |   0  | Bksp |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |   `  |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |   -  |   =  |   [  |   ]  |  \   |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |  F7  |  F8  |  F9  |  F10 |  F11 |  F12 |      |      | Prev | Vol+ | Next |
 * |------+------+------+---x--+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |             |      |      | Next | Vol- | Play |
 * `-----------------------------------------------------------------------------------'
 */
[_RAISE] = {
  {KC_DEL,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_BSPC},
  {KC_GRV,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_MINS, KC_EQL,  KC_LBRC, KC_RBRC, KC_BSLS},
  {_______, KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  _______, _______, KC_MPRV, KC_VOLU, KC_MNXT},
  {_______, _______, _______, _______, _______, _______, _______, _______, _______, KC_MNXT, KC_VOLD, KC_MPLY}
},

/* Adjust (Lower + Raise)
 * ,-----------------------------------------------------------------------------------.
 * | Slep | Reset|      |      |      |      |      |      |      |      | Ins  |  Del |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * | Caps |      |      |      |      |AGnorm|AGswap|      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Caps |      |      |      |      |      |      |      |      | MC1R | MCRS | MC1P |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * | Brite|      |      |      |      |             |      |      | MC2R | MCRS | MC2P |
 * `-----------------------------------------------------------------------------------'
 */
[_ADJUST] = {
  {KC_SLEP, RESET,   DEBUG,   _______, _______, _______, _______, TERM_ON, TERM_OFF,_______,        KC_INS,       KC_DEL },
  {KC_CAPS, _______, _______, _______, _______, AG_NORM, AG_SWAP, _______, _______, _______,        _______,      _______},
  {KC_CAPS, _______, _______, _______, _______, _______, _______, _______, _______, DYN_REC_START1, DYN_REC_STOP, DYN_MACRO_PLAY1},
  {BACKLIT, _______, _______, _______, _______, _______, _______, _______, _______, DYN_REC_START2, DYN_REC_STOP, DYN_MACRO_PLAY2}
},

/* Mouse Layer
 * ,----------------------------------------------------------------------------------------------------.
 * |      | Alt1     | Sup2     | Sup3      |AltF4     |      |      |AltF7 |  Up  |      |AltIns|      |
 * |------+----------+----------+-----------+----------+-------------+------+------+------+------+------|
 * |      |  :)      |  :(      |  :/       | o_O      |      |      | Left | Down |Right | M3Bt |      |
 * |------+----------+----------+-----------+----------+------|------+------+------+------+------+------|
 * |      | ♥        | ಠ_ಠ      |( ͡° ͜ʖ ͡°)|¯\_(ツ)_/¯|      |      |      | MWUp | M1Bt | MoUp | M2Bt |
 * |------+----------+----------+-----------+----------+------+------+------+------+------+------+------|
 * | Acc0 | Acc1     | Acc2     |           |          |             |      | MWDn | MoLe | MoDn | MoRi |
 * `----------------------------------------------------------------------------------------------------'
 */
[_MOUSE] = {
  {_______, LALT(KC_1), LGUI(KC_2), LGUI(KC_3), LALT(KC_F4), _______, _______, LALT(KC_F7), KC_UP,   _______, LALT(KC_INS),  _______},
  {_______, SMILE,      SAD,        HMMM,       CONFUSED,    _______, _______, KC_LEFT,     KC_DOWN, KC_RGHT, KC_BTN3,       _______},
  {_______, UC(L'♥'),   DISSAPROVAL,LENNY,      SHRUG,       _______, _______, _______,     KC_WH_U, KC_BTN1, KC_MS_U,       KC_BTN2},
  {KC_ACL0, KC_ACL1,    KC_ACL2,    _______,    _______,     _______, _______, _______,     KC_WH_D, KC_MS_L, KC_MS_D,       KC_MS_R}
}


};
	

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (!process_record_dynamic_macro(keycode, record)) {
	return false;
  }
  switch (keycode) {
    case QWERTY:
      if (record->event.pressed) {
        set_single_persistent_default_layer(_QWERTY);
        //breathing_self_disable();
      }
      return false;
      break;
	case DISSAPROVAL:
	  if (record->event.pressed) {
        process_unicode((L'ಠ'|QK_UNICODE), record);   // Eye
		SEND_STRING("_");                             // Nose
        process_unicode((L'ಠ'|QK_UNICODE), record);   // Eye
	  }
	  return false;
	  break;
	case LENNY:
	  if (record->event.pressed) {
		//( ͡° ͜ʖ ͡°)
		SEND_STRING("(");                             // 
        process_unicode((L' '|QK_UNICODE), record);   // 
        process_unicode((L'͡'|QK_UNICODE), record);   // 
        process_unicode((L'°'|QK_UNICODE), record);   // 
        process_unicode((L' '|QK_UNICODE), record);   // 
        process_unicode((L'͜'|QK_UNICODE), record);   // 
        process_unicode((L'ʖ'|QK_UNICODE), record);   // 
        process_unicode((L' '|QK_UNICODE), record);   // 
        process_unicode((L'͡'|QK_UNICODE), record);   // 
        process_unicode((L'°'|QK_UNICODE), record);   // 
		SEND_STRING(")");                             // 
	  }
	  return false;
	  break;
	case SMILE:
	  if (record->event.pressed) {
		SEND_STRING(":)");
	  }
	  return false;
	  break;
	case SAD:
	  if (record->event.pressed) {
		SEND_STRING(":(");
	  }
	  return false;
	  break;
	case CONFUSED:
	  if (record->event.pressed) {
		SEND_STRING("o_O");
	  }
	  return false;
	  break;
	case HMMM:
	  if (record->event.pressed) {
		SEND_STRING(":/");
	  }
	  return false;
	  break;
	case SHRUG:
	  if (record->event.pressed) {
        process_unicode((0x00AF|QK_UNICODE), record);   // Hand
        tap(KC_BSLS);                                   // Arm
        register_code(KC_RSFT);
        tap(KC_UNDS);                                   // Arm
        tap(KC_LPRN);                                   // Head
        unregister_code(KC_RSFT);
        process_unicode((0x30C4|QK_UNICODE), record);   // Face
        register_code(KC_RSFT);
        tap(KC_RPRN);                                   // Head
        tap(KC_UNDS);                                   // Arm
        unregister_code(KC_RSFT);
        tap(KC_SLSH);                                   // Arm
        process_unicode((0x00AF|QK_UNICODE), record);   // Hand
	  }
	  return false;
	  break;
    case LOWER:
      if (record->event.pressed) {
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
        /*uint8_t default_layer = biton32(default_layer_state);
		if (default_layer == _QWERTY) {
          #ifdef BACKLIGHT_ENABLE
            breathing_period_set(5);
            breathing_enable();
          #endif
        }*/
        layer_on(_LOWER);
      } else {
        layer_off(_LOWER);
		/*uint8_t default_layer = biton32(default_layer_state);
        if (default_layer == _QWERTY) {
          breathing_self_disable();
        }*/
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
        /*uint8_t default_layer = biton32(default_layer_state);
        if (default_layer == _QWERTY) {
          #ifdef BACKLIGHT_ENABLE
            breathing_period_set(2);
            breathing_enable();
          #endif
        }*/
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_RAISE);
        /*uint8_t default_layer = biton32(default_layer_state);
        if (default_layer == _QWERTY) {
          breathing_self_disable();
        }*/
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case MOUSE:
      if (record->event.pressed) {
        layer_on(_MOUSE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_MOUSE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case BACKLIT:
      if (record->event.pressed) {
        register_code(KC_RSFT);
        #ifdef BACKLIGHT_ENABLE
          backlight_step();
        #endif
      } else {
        unregister_code(KC_RSFT);
      }
      return false;
      break;
  }
  return true;
}